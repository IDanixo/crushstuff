﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace MiniEngine
{
    public class MoveTo : Component
    {
        public delegate void MoveToEvent(Transform transform);
        public event MoveToEvent OnReachedPosition;
        public Vector2 Target { get { return target; } }

        public float Speed = 1;

        private Transform transform;
        private Renderer renderer;
        private Random random = new Random();

        private Vector2 target = Vector2.Zero;
        private Vector2 moveDirection = Vector2.Zero;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            if (transform == null)
                throw new Exception("GameObject needs a Transform component");

            renderer = GameObject.GetComponent<Renderer>();
            if (renderer == null)
                throw new Exception("GameObject needs a Renderer component");

            EventManager.OnUpdate += OnUpdate;
        }

        void OnUpdate(GameTime gameTime)
        {
            Move();
        }

        public void SetTarget(Vector2 target)
        {
            this.target = target;
            moveDirection = target - transform.Position;
            moveDirection.Normalize();
        }

        public void Move()
        {
            if (target != Vector2.Zero)
            {
                float distance = Vector2.Distance(transform.Position, target);
                if (distance > 2)
                    transform.Translate(moveDirection * Speed);
                else
                    transform.Position = target;
            }
        }

        private void SetRandomTarget()
        {
            // Should be window width
            int width = 1280;
            // Should be window height
            int height = 720;

            int x = 0;
            int y = 0;

            // Moves to a focused X Coordinate if true
            int xTrue = random.Next(2);
            // Moves to a Side or the opposite
            int side = random.Next(2);

            if(xTrue == 0)
            {
                if(side == 0)
                {
                    //Top Side Random X
                    x = random.Next(width + 1);

                    //To get a Gameobject out of screen
                    y = renderer.ImageHeight;

                    x = y * (x / width) + x;
                    y = -y;
                }
                else
                {
                    //Down Side Random X
                    x = random.Next(width + 1);
                    y = height;
                }
            }
            else
            {
                if(side == 0)
                {
                    //Left Side Random Y
                    y = random.Next(height + 1);
                    x = renderer.ImageWidth;

                    y = x * (y / height) + y;
                    x = -x;
                }
                else
                {
                    //Right Side Random Y
                    y = random.Next(height + 1);
                    x = width;
                }
            }

            target = new Vector2(x , y);
        }

        public void MoveRandom(float speed)
        {
            this.Speed = speed;

            SetRandomTarget();
            moveDirection = target - transform.Position;
            moveDirection.Normalize();
        }

        public void Stop()
        {
            target = Vector2.Zero;
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;
            base.Destroy();
        }
    }
}
