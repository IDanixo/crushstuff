﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace MiniEngine
{
    public class BoxCollider : Component
    {
        public delegate void CollisionEvent(BoxCollider other);
        public event CollisionEvent OnCollisionEnter, OnCollisionStay, OnCollisionExit;

        public int width, height;
        public Rectangle bounds;

        private List<BoxCollider> collisions = new List<BoxCollider>();

        private MouseState mouse;

        private Transform transform;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            if (transform == null)
                throw new Exception("GameObject needs a Transform component");

            UpdatePosition();

            CollisionManager.AddCollider(this);
            BoundsToRendereSize();

            EventManager.OnLateUpdate += OnLateUpdate;
        }

        public void SetCursorUpdate()
        {
            EventManager.OnLateUpdate -= OnLateUpdate;
            EventManager.OnLateUpdate += OnLateCursorUpdate;
        }

        private void OnLateUpdate(GameTime gameTime)
        {
            UpdatePosition();
        }

        private void OnLateCursorUpdate(GameTime gameTime)
        {
            UpdateCursorPosition();
        }

        public void SetSize(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public void CheckCollision(BoxCollider other)
        {
            if (bounds.Intersects(other.bounds))
            {
                if (!collisions.Contains(other))
                {
                    collisions.Add(other);
                    if (OnCollisionEnter != null)
                        OnCollisionEnter(other);
                }
                else
                {
                    if (OnCollisionStay != null)
                        OnCollisionStay(other);
                }
            }
            else
            {
                BoxCollider exitCollider = collisions.Find((BoxCollider collider) => collider == other);
                if (exitCollider != null)
                {
                    collisions.Remove(exitCollider);
                    if (OnCollisionExit != null)
                        OnCollisionExit(other);
                }
            }
        }

        private void UpdatePosition()
        {
            bounds.X = (int)transform.Position.X;
            bounds.Y = (int)transform.Position.Y;
        }

        private void UpdateCursorPosition()
        {
            mouse = Mouse.GetState();

            bounds.X = mouse.X;
            bounds.Y = mouse.Y;
        }

        public void BoundsToRendereSize()
        {
            Renderer renderer = GameObject.GetComponent<Renderer>();
            if (renderer != null)
            {
                bounds.Width = renderer.ImageWidth;
                bounds.Height = renderer.ImageHeight;
            }
        }

        public override void Destroy()
        {
            CollisionManager.RemoveCollider(this);
            EventManager.OnLateUpdate -= OnLateUpdate;
            base.Destroy();
        }
    }
}
