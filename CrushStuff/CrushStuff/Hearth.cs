﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class Hearth : GameObject
    {
        Transform transform;
        Renderer renderer;

        public static int Hearts = 3;

        public Hearth()
        {
            Hearts = 3;

            transform = AddComponent<Transform>();
            transform.Position = new Vector2(1200, 25);

            renderer = AddComponent<Renderer>();
            renderer.SetImage(Managers.Content.Load<Texture2D>("Sprites/Hearth2"));
            renderer.PositionZ = 0.2f;

            EventManager.OnUpdate += OnUpdate;
        }

        private void OnUpdate(GameTime gameTime)
        {
        }

        public override void Destroy()
        {
            base.Destroy();
        }
    }
}
