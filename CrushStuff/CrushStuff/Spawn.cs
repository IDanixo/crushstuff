﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;

//Autor: Daniel Mielnicki

namespace CrushStuff
{
    class Spawn : GameObject
    {
        private Vector2 spawn;
        private Transform transform;
        private Renderer renderer;
        private GameObject gameObject;

        private static float spawnDelayTankEnemy = 20000f;
        private float remainingDelayTankEnemy = spawnDelayTankEnemy;

        private static float spawnDelayFastEnemy = 40000f;
        private float remainingDelayFastEnemy = spawnDelayFastEnemy;

        private static float spawnDelayMiniEnemy = 4000f;
        private float remainingDelayMiniEnemy = spawnDelayMiniEnemy;

        private static float spawnDelayEnemy = 11000f;
        private float remainingDelayEnemy = spawnDelayEnemy;

        public Spawn()
        {
            //this.spawnDelay = spawnDelay;
            ////spawn = new Vector2(Managers.Graphics.PreferredBackBufferWidth / 2, Managers.Graphics.PreferredBackBufferHeight / 2);

            //transform = gameObject.GetComponent<Transform>();
            //if (transform == null)
            //    throw new Exception("[Spawn]A GameObject needs Transform!.");

            //renderer = gameObject.GetComponent<Renderer>();
            //if (renderer == null)
            //    throw new Exception("[Spawn]A GameObject needs Renderer!.");

            //transform.Position = new Vector2((Managers.Graphics.PreferredBackBufferWidth / 2) - (renderer.ImageWidth / 2), (Managers.Graphics.PreferredBackBufferHeight / 2) - (renderer.ImageHeight / 2));

            EventManager.OnUpdate += OnUpdate;
        }

        private void OnUpdate(GameTime gameTime)
        {
            var timer = (float)gameTime.ElapsedGameTime.Milliseconds;
            remainingDelayEnemy -= timer;
            remainingDelayMiniEnemy -= timer;
            remainingDelayTankEnemy -= timer;
            remainingDelayFastEnemy -= timer;

            if (remainingDelayFastEnemy <= 0)
            {
                new TankEnemy("Sprites/SpeedFly22", 3f);
                remainingDelayFastEnemy = spawnDelayFastEnemy;
            }

            if (remainingDelayTankEnemy <= 0)
            {
                new TankEnemy("Sprites/TankEnemy", 1f);
                remainingDelayTankEnemy = spawnDelayTankEnemy;
            }

            if (remainingDelayMiniEnemy <= 0)
            {
                new MiniEnemy("Sprites/MiniFly22", 1f, new Vector2(640, 350));
                remainingDelayMiniEnemy = spawnDelayMiniEnemy;
            }

            if(remainingDelayEnemy <= 0)
            {
                new Enemy("Sprites/Fly25", 1f);
                remainingDelayEnemy = spawnDelayEnemy;
            }


        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
