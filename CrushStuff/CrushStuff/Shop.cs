﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    abstract class Shop : GameObject
    {

        protected Renderer renderer;
        protected Transform transform;
        protected BoxCollider collider;
        protected Health health;

        protected string spritePath;

        public Shop()
        {
            transform = AddComponent<Transform>();

            health = AddComponent<Health>();

            collider = AddComponent<BoxCollider>();

            renderer = AddComponent<Renderer>();

            Console.WriteLine("Shop wurde erstellt");
        }

        public override void Destroy()
        {
            base.Destroy();
        }
    }
}
