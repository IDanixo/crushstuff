﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class EnchantScroll : Shop
    {
        public EnchantScroll()
        {
            Tag = "Scroll";
            transform.Position = new Vector2(0, 350);
            spritePath = "Sprites/Enchant";
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            renderer.PositionZ = 0.2f;
            collider.BoundsToRendereSize();
            collider.OnCollisionEnter += OnCollisionEnter;
            collider.OnCollisionStay += OnCollisionStay;
            collider.OnCollisionExit += OnCollisionExit;
            EventManager.OnUpdate += OnUpdate;
        }

        private void IsPinSelected()
        {
            //if (/*IsSelected == true*/)
            //{
            //    spritePath = "Sprites/Scroll_Selected";
            //    renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            //}
        }

        private void OnHealthZero()
        {
        }

        private void OnCollisionEnter(BoxCollider other)
        {
            Console.WriteLine("YAY");
        }

        private void OnCollisionStay(BoxCollider other)
        {
        }


        private void OnCollisionExit(BoxCollider other)
        {
        }

        private void OnUpdate(GameTime gameTime)
        {
            IsPinSelected();
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
