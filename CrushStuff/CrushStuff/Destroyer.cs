﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;

namespace CrushStuff
{
    class Destroyer : GameObject
    {
        Transform transform;
        BoxCollider collider;


        public Destroyer()
        {
            Tag = "Player";

            transform = AddComponent<Transform>();
            transform.Position = new Vector2(0, 0);

            collider = AddComponent<BoxCollider>();
            collider.SetSize(1, 1);
            collider.OnCollisionEnter += OnCollisionEnter;

            Console.WriteLine("DESTROYER POS" + transform.Position);
        }

        private void OnCollisionEnter(BoxCollider other)
        {
            Console.WriteLine("TRUE DESTROYER");
            other.GameObject.Destroy();
        }
    }
}
