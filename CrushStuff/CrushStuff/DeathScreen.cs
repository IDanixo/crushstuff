﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrushStuff
{
    class DeathScreen : Scene
    {
        private Texture2D image;

        public override void LoadContent()
        {
            image = Managers.Content.Load<Texture2D>("Sprites/DeathScreen");
        }

        public override void Update(GameTime gameTime)
        {
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, image.Bounds, Color.White);
        }
    }
}
