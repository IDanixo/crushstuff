﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MiniEngine;

namespace CrushStuff
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //Texture2D image;
        //Texture2D gold;
        //Enemy enemy;
        //Cheese cheese;
        //Boss boss;
        //Hearth hearth;
        //Spawn spawn;
        //Pin pin;
        //Pan pan;
        //Shop shop;
        //EnchantScroll scroll;
        //Cursor cursor;
        //Destroyer destroyer;
        //SpriteFont font;

        bool IsLoaded;
        bool IsDeathScreenLoaded;
        bool IsWinScreenLoaded;

        private const float spawnDelay = 5000f;
        private float remainingDelay = spawnDelay;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            Managers.Content = this.Content;
            Managers.Graphics = this.graphics;

            SceneManager.LoadScene<StartScene>();

            //background = Content.Load<Texture2D>("Sprites/Background");

            //AB HIER
            //gold = Content.Load<Texture2D>("Sprites/Gold3");

            //font = Content.Load<SpriteFont>("Fonts/Arial");

            //cheese = new Cheese();
            //hearth = new Hearth();

            //pin = new Pin();
            //pan = new Pan();
            //scroll = new EnchantScroll();

            spriteBatch = new SpriteBatch(GraphicsDevice);

            //spawn = new Spawn();

            //cursor = new Cursor();
            //BIS HIER

            //destroyer = new Destroyer();

            //cursor = new Cursor();
            //enemy = new Enemy("Sprites/Fly2", 1f);
            //new Enemy("Sprites/Enemy", 1f);
            //new Enemy("Sprites/Enemy", 1f);



            //new PinWeapon();
            //boss = new Boss("Sprites/Bubble");
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            //WorldSetting.Timer(gameTime);
            //EventManager.Update(gameTime);
            SceneManager.Update(gameTime);

            MouseState mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed && IsLoaded == false)
            {
                SceneManager.LoadScene<LevelScene>();
                IsLoaded = true;
            }

           

            if (Hearth.Hearts <= 0 && IsDeathScreenLoaded == false || IsDeathScreenLoaded == true)
            {
                IsDeathScreenLoaded = true;
                SceneManager.LoadScene<DeathScreen>();

                var timer = (float)gameTime.ElapsedGameTime.Milliseconds;
                remainingDelay -= timer;

                if (remainingDelay <= 0)
                {
                    IsDeathScreenLoaded = false;
                    SceneManager.LoadScene<StartScene>();
                }
            }
                

            base.Update(gameTime);

        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);


            base.Draw(gameTime);

            spriteBatch.Begin(SpriteSortMode.FrontToBack);
            SceneManager.Draw(spriteBatch);
            spriteBatch.End();

            //spriteBatch.Begin(SpriteSortMode.FrontToBack);
            //spriteBatch.DrawString(font, Hearth.Hearts.ToString(), new Vector2(1240, 25), Color.Black);
            //spriteBatch.DrawString(font, "1500", new Vector2(30, 195), Color.Black, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
            //spriteBatch.DrawString(font, "3000", new Vector2(30, 297), Color.Black, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
            //spriteBatch.DrawString(font, Gold.myGold.ToString(), new Vector2(1050, 25), Color.Black);
            //spriteBatch.Draw(gold, new Vector2(1000, 25), Color.White);
            //EventManager.Render(spriteBatch);
            //spriteBatch.End();
        }
    }
}
