﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class Cheese : GameObject
    {
        Transform transform;
        Renderer renderer;
        BoxCollider collider;
        Health health;

        public Cheese()
        {
            health = AddComponent<Health>();
            health.MaxHealth = 1000;
            health.CurrentHealth = 1000;
            health.OnHealthZero += OnHealthZero;

            transform = AddComponent<Transform>();
            transform.Position = new Vector2(550, 270);

            renderer = AddComponent<Renderer>();
            renderer.SetImage(Managers.Content.Load<Texture2D>("Sprites/Cheese"));
            renderer.PositionZ = 0.9f;

            collider = AddComponent<BoxCollider>();
            collider.BoundsToRendereSize();

            collider.OnCollisionStay += OnCollisionStay;

            EventManager.OnUpdate += OnUpdate;
        }

        private void OnUpdate(GameTime gameTime)
        {
        }

        private void OnHealthZero()
        {
            Destroy();
        }

        private void OnCollisionStay(BoxCollider other)
        {
        }

        public override void Destroy()
        {
            health.OnHealthZero -= OnHealthZero;
            base.Destroy();
        }
    }
}
