﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrushStuff
{
    class PinWeapon : GameObject
    {
        MouseState mouse;

        Transform transform;
        Renderer renderer;
        //BoxCollider collider;
        Texture2D texture2D;

        private string spritePath;
        public static bool mySelection;
        private bool isBought = false;
        private int cost = 100;
        private bool isReleased = false;

        public PinWeapon()
        {
            Tag = "PinWeapon";

            transform = AddComponent<Transform>();
            //transform.Position = new Vector2(0, 150);
            renderer = AddComponent<Renderer>();
            //collider = AddComponent<BoxCollider>();
            ////collider.BoundsToRendereSize();
            //collider.OnCollisionEnter += OnCollisionEnter;
            //collider.OnCollisionStay += OnCollisionStay;
            //collider.OnCollisionExit += OnCollisionExit;
            EventManager.OnUpdate += OnUpdate;
        }

        public void Start()
        {
            spritePath = "Sprites/PinWeapon";
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
        }

        public void End()
        {
            spritePath = "Sprites/Nichts";
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
        }

        //private void OnRender(SpriteBatch spriteBatch)
        //{
        //    spriteBatch.Draw(spriteBatch, )
        //}

        //private void OnHealthZero()
        //{
        //}

        private void OnCollisionEnter(BoxCollider other)
        {
            Console.WriteLine("YAY");
        }

        private void OnCollisionStay(BoxCollider other)
        {
        }


        private void OnCollisionExit(BoxCollider other)
        {
        }

        public static void Attack(BoxCollider other)
        {
            Health health = other.GameObject.GetComponent<Health>();
            health.RemoveHealth(WorldSetting.PinDamage);
        }

        private void OnUpdate(GameTime gameTime)
        {
            mouse = Mouse.GetState();
            transform.Position = new Vector2(mouse.X, mouse.Y);
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
