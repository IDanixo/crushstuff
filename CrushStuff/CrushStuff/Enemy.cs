﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrushStuff
{
    class Enemy : GameObject
    {
        Renderer renderer;
        Transform transform;
        BoxCollider collider;
        MoveTo moveTo;
        Health health;
        Random random;

        Vector2 deathPosition;

        private float Speed;
        private string spritePath;

        public Enemy(string spritePath, float speed)
        {
            Tag = "Enemy";

            this.spritePath = spritePath;
            this.Speed = speed;
            transform = AddComponent<Transform>();

            health = AddComponent<Health>();
            health.MaxHealth = 10;
            health.CurrentHealth = 5;
            health.OnHealthZero += OnHealthZero;

            renderer = AddComponent<Renderer>();
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            renderer.PositionZ = 0.5f;

            transform.Position = new Vector2((Managers.Graphics.PreferredBackBufferWidth / 2) - (renderer.ImageWidth / 2), (Managers.Graphics.PreferredBackBufferHeight / 2) - (renderer.ImageHeight / 2));

            collider = AddComponent<BoxCollider>();
            collider.BoundsToRendereSize();
            collider.OnCollisionEnter += OnCollisionEnter;
            collider.OnCollisionStay += OnCollisionStay;
            collider.OnCollisionExit += OnCollisionExit;

            moveTo = AddComponent<MoveTo>();
            moveTo.MoveRandom(Speed);
            moveTo.OnReachedPosition += OnReachedPosition;

            EventManager.OnUpdate += OnUpdate;
        }

        private void OnHealthZero()
        {
            MiniEnemy.spawnMini = true;
            Destroy();
        }

        private void OnReachedPosition(Transform transform)
        {
            if (moveTo.Target == transform.Position)
            {
                Hearth.Hearts = Hearth.Hearts - 1;
                Destroy();
            }
        }
        
        private void OnCollisionEnter(BoxCollider other)
        {
            if (other.GameObject.Tag != "Pin" && other.GameObject.Tag != "Pan" && other.GameObject.Tag != "Scroll" )
            {
                moveTo.MoveRandom(Speed);
                //transform.LookAt(moveTo.Target);
                //Console.WriteLine("ROTATION: " + transform.Rotation);
            }
        }

        private void OnCollisionStay(BoxCollider other)
        {

        }


        private void OnCollisionExit(BoxCollider other)
        {
        }

        private void OnUpdate(GameTime gameTime)
        {
            SpawnMiniEnemy(gameTime);
            OnReachedPosition(transform);
        }

        public void SpawnMiniEnemy(GameTime gameTime)
        {

            if (MiniEnemy.spawnMini == true)
            {
                new MiniEnemy("Sprites/MiniFly22", 1f, new Vector2(MiniEnemy.spawnPosition.X, MiniEnemy.spawnPosition.Y));
                new MiniEnemy("Sprites/MiniFly22", 1f, new Vector2(MiniEnemy.spawnPosition.X + 20, MiniEnemy.spawnPosition.Y + 20));
                MiniEnemy.spawnMini = false;
            }
        }

        public override void Destroy()
        {
            deathPosition = transform.Position;
            MiniEnemy.spawnPosition = deathPosition;
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
