﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class Pin : Shop
    {
        GraphicsDevice graphics;
        PinWeapon pinWeapon;

        public static bool mySelection;
        public static bool isBought;
        private int cost = 100;

        private string spriteSelectedNotBought = "Sprites/Pin_Weapon_nbs";
        private string spriteNotSelectedNotBought = "Sprites/Pin_Weapon_ns";
        private string spriteNotSelectedBought = "Sprites/Pin_Weapon_s";
        private string spriteSelectedBought = "Sprites/Pin_Weapon_sb";

        public Pin()
        {
            Tag = "Pin";
            pinWeapon = new PinWeapon();
            transform.Position = new Vector2(0, 150);
            spritePath = spriteNotSelectedNotBought;
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            renderer.PositionZ = 0.2f;
            collider.BoundsToRendereSize();
            collider.OnCollisionEnter += OnCollisionEnter;
            collider.OnCollisionStay += OnCollisionStay;
            collider.OnCollisionExit += OnCollisionExit;
            EventManager.OnUpdate += OnUpdate;
        }

        private void IsPinSelected()
        {
            if (isBought == true)
            {
                if(mySelection != true)
                {
                    spritePath = spriteNotSelectedBought;
                    renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
                    pinWeapon.End();
                }
                else
                {
                    Cursor.spritePath = "Sprites/Nichts";
                    spritePath = spriteSelectedBought;
                    renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
                    pinWeapon.Start();
                }
            }
            else if (mySelection == true && Gold.myGold >= cost)
            {
                Gold.myGold -= cost;
                isBought = true;

                spritePath = spriteSelectedBought;
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            }
            else if (mySelection == true)
            {
                spritePath = spriteSelectedNotBought;
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            }
            else
            {
                spritePath = spriteNotSelectedNotBought;
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            }
        }

        private void OnHealthZero()
        {
        }

        private void OnCollisionEnter(BoxCollider other)
        {
            Console.WriteLine("YAY");
        }

        private void OnCollisionStay(BoxCollider other)
        {
        }


        private void OnCollisionExit(BoxCollider other)
        {
        }

        private void OnUpdate(GameTime gameTime)
        {
            IsPinSelected();
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
