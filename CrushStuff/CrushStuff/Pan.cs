﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class Pan : Shop
    {
        public static bool mySelection;
        public static bool isBought;
        private int cost = 200;

        private string spriteSelectedNotBought = "Sprites/Gun_Weapon_nbs";
        private string spriteNotSelectedNotBought = "Sprites/Gun_Weapon_ns";
        private string spriteNotSelectedBought = "Sprites/Gun_Weapon_s";
        private string spriteSelectedBought = "Sprites/Gun_Weapon_sb";

        public Pan()
        {
            Tag = "Pan";
            transform.Position = new Vector2(0, 250);
            spritePath = "Sprites/Pan";
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            renderer.PositionZ = 0.2f;
            collider.BoundsToRendereSize();
            collider.OnCollisionEnter += OnCollisionEnter;
            collider.OnCollisionStay += OnCollisionStay;
            collider.OnCollisionExit += OnCollisionExit;
            EventManager.OnUpdate += OnUpdate;
        }

        private void IsPanSelected()
        {
            if (isBought == true)
            {
                if (mySelection != true)
                {
                    spritePath = spriteNotSelectedBought;
                    renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
                }
                else
                {
                    spritePath = spriteSelectedBought;
                    renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
                }
            }
            else if (mySelection == true && Gold.myGold >= cost)
            {
                Gold.myGold -= cost;
                isBought = true;

                spritePath = spriteSelectedBought;
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            }
            else if (mySelection == true)
            {
                spritePath = spriteSelectedNotBought;
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            }
            else
            {
                spritePath = spriteNotSelectedNotBought;
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            }
        }

        private void OnHealthZero()
        {
        }

        private void OnCollisionEnter(BoxCollider other)
        {
            Console.WriteLine("YAY");
        }

        private void OnCollisionStay(BoxCollider other)
        {
        }


        private void OnCollisionExit(BoxCollider other)
        {
        }

        private void OnUpdate(GameTime gameTime)
        {
            IsPanSelected();
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
