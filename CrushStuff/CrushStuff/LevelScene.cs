﻿using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class LevelScene : Scene
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D image;
        Texture2D gold;
        Enemy enemy;
        Cheese cheese;
        Boss boss;
        Hearth hearth;
        Spawn spawn;
        Pin pin;
        Pan pan;
        Shop shop;
        EnchantScroll scroll;
        Cursor cursor;
        Destroyer destroyer;
        SpriteFont font;

        public override void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Begin(SpriteSortMode.FrontToBack);
            spriteBatch.DrawString(font, Hearth.Hearts.ToString(), new Vector2(1240, 25), Color.Black);
            spriteBatch.DrawString(font, "1500", new Vector2(30, 195), Color.Black, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(font, "3000", new Vector2(30, 297), Color.Black, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(font, Gold.myGold.ToString(), new Vector2(1050, 25), Color.Black);
            spriteBatch.Draw(gold, new Vector2(1000, 25), Color.White);
            EventManager.Render(spriteBatch);
            //spriteBatch.End();
        }

        public override void LoadContent()
        {
            gold = Managers.Content.Load<Texture2D>("Sprites/Gold3");

            font = Managers.Content.Load<SpriteFont>("Fonts/Arial");

            cheese = new Cheese();
            hearth = new Hearth();

            pin = new Pin();
            pan = new Pan();
            scroll = new EnchantScroll();

            spawn = new Spawn();

            cursor = new Cursor();
        }

        public override void Update(GameTime gameTime)
        {
            EventManager.Update(gameTime);
        }
    }
}
