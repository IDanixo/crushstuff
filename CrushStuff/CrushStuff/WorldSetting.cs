﻿using Microsoft.Xna.Framework;
using System;

namespace CrushStuff
{
    public static class WorldSetting
    {
        public static Vector2 CursorPosition;
        public static int PinDamage = 10;
        public static int PanDamage = 2;
        public static int PlayerHealth = 3;
    }
}
