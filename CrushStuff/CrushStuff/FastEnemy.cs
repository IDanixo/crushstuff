﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrushStuff
{
    class FastEnemy : GameObject
    {
        public static bool spawnMini;
        public static Vector2 spawnPosition;

        Renderer renderer;
        Transform transform;
        BoxCollider collider;
        MoveTo moveTo;
        Health health;
        Random random = new Random();
        Vector2 deathPosition;


        private int i;
        private float Speed;
        private string spritePath;

        public FastEnemy(string spritePath, float speed)
        {
            this.spritePath = spritePath;
            this.Speed = speed;
            transform = AddComponent<Transform>();

            health = AddComponent<Health>();
            health.MaxHealth = 100;
            health.CurrentHealth = 3;
            health.OnHealthZero += OnHealthZero;

            renderer = AddComponent<Renderer>();
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            renderer.PositionZ = 0.5f;

            transform.Position = new Vector2((Managers.Graphics.PreferredBackBufferWidth / 2) - (renderer.ImageWidth / 2), (Managers.Graphics.PreferredBackBufferHeight / 2) - (renderer.ImageHeight / 2));

            collider = AddComponent<BoxCollider>();
            collider.BoundsToRendereSize();
            collider.OnCollisionEnter += OnCollisionEnter;
            collider.OnCollisionStay += OnCollisionStay;
            collider.OnCollisionExit += OnCollisionExit;

            moveTo = AddComponent<MoveTo>();
            moveTo.MoveRandom(Speed);
            moveTo.OnReachedPosition += OnReachedPosition;

            Console.WriteLine("My Position: " + transform.Position);

            EventManager.OnUpdate += OnUpdate;
        }

        private void OnHealthZero()
        {
            i = random.Next(2);
            if (i == 0)
            {
                Gold.spawnGold = true;
            }
            Destroy();
        }

        private void OnReachedPosition(Transform transform)
        {
            if (moveTo.Target == transform.Position)
            {
                Hearth.Hearts--;
                Destroy();
            }
        }

        private void OnCollisionEnter(BoxCollider other)
        {
            moveTo.MoveRandom(Speed);
        }

        private void OnCollisionStay(BoxCollider other)
        {

        }


        private void OnCollisionExit(BoxCollider other)
        {
        }

        private void OnUpdate(GameTime gameTime)
        {
            OnReachedPosition(transform);
            SpawnGold(gameTime);
        }

        private void SpawnGold(GameTime gameTime)
        {
            if (Gold.spawnGold == true)
            {
                new Gold(new Vector2(Gold.spawnPosition.X, Gold.spawnPosition.Y));
                Gold.spawnGold = false;
            }
        }

        public override void Destroy()
        {
            deathPosition = transform.Position;
            Gold.spawnPosition = deathPosition;
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
