﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class Gold : GameObject
    {
        //For the spawn
        public static bool spawnGold;
        public static Vector2 spawnPosition;
        public static int myGold;


        Renderer renderer;
        Transform transform;
        BoxCollider collider;
        Health health;
        Random random = new Random();

        private string spritePath;

        private int randomNumber;
        private const float destroyDelay = 5000f;
        private float remainingDelay = destroyDelay;

        public Gold(Vector2 position)
        {
            Tag = "Gold";
            myGold = 1000;
            transform = AddComponent<Transform>();
            transform.Position = position;
            spritePath = "Sprites/Gold2";
            renderer = AddComponent<Renderer>();
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            renderer.PositionZ = 0.5f;
            collider = AddComponent<BoxCollider>();
            collider.BoundsToRendereSize();

            health = AddComponent<Health>();
            health.MaxHealth = 1;
            health.CurrentHealth = 1;
            health.OnHealthZero += OnHealthZero;

            EventManager.OnUpdate += OnUpdate;
        }

        private void OnHealthZero()
        {
            randomNumber = random.Next(50);
            myGold += randomNumber;
            Console.WriteLine("MY GOLD: " + myGold);
            Destroy();
        }

        private void OnUpdate(GameTime gameTime)
        {
            var timer = (float)gameTime.ElapsedGameTime.Milliseconds;
            remainingDelay -= timer;

            if (remainingDelay <= 0)
            {
                Destroy();
                remainingDelay = destroyDelay;
            }
        }

        private void DestroyInTime()
        {

        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
