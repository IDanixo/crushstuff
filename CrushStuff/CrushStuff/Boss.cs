﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrushStuff
{
    class Boss : GameObject
    {
        Renderer renderer;
        Transform transform;
        BoxCollider collider;
        MouseState mouse;

        public Boss(string spritePath)
        {
            Tag = "Boss";

            transform = AddComponent<Transform>();

            renderer = AddComponent<Renderer>();
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));

            transform.Position = new Vector2((Managers.Graphics.PreferredBackBufferWidth / 2) - (renderer.ImageWidth / 2), (Managers.Graphics.PreferredBackBufferHeight / 2) - (renderer.ImageHeight / 2));

            collider = AddComponent<BoxCollider>();
            collider.BoundsToRendereSize();
            collider.OnCollisionEnter += OnCollisionEnter;
            collider.OnCollisionStay += OnCollisionStay;


            EventManager.OnUpdate += OnUpdate;
        }

        private void OnCollisionStay(BoxCollider other)
        {
            mouse = Mouse.GetState();

            if (mouse.LeftButton == ButtonState.Pressed)
            {
                Destroy();
            }
        }

        private void OnCollisionEnter(BoxCollider other)
        {
            mouse = Mouse.GetState();

            if (mouse.LeftButton == ButtonState.Pressed)
            {
                Destroy();
            }
        }

        private void OnUpdate(GameTime gameTime)
        {
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }
    }
}
