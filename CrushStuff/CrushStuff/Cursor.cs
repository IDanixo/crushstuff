﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrushStuff
{
    class Cursor : GameObject
    {
        Transform transform;
        BoxCollider collider;
        Renderer renderer;
        MouseState mouse;

        bool isReleased = false;
        bool isRendered = false;
        public static string spritePath = "Sprites/Arrow4";

        public Cursor()
        {
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            collider = AddComponent<BoxCollider>();

            collider.SetCursorUpdate();
            collider.SetSize(1, 1);
            collider.OnCollisionEnter += OnCollisionEnter;
            collider.OnCollisionStay += OnCollisionStay;
            collider.OnCollisionExit += OnCollisionExit;

            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
            renderer.PositionZ = 0.9f;

            EventManager.OnUpdate += OnUpdate;
        }

        private void OnCollisionExit(BoxCollider other)
        {
            //Console.WriteLine("Collision Exit!");
        }

        private void OnCollisionEnter(BoxCollider other)
        {
            Console.WriteLine(other.GameObject.Tag);
            CheckCollision(other);
            //Console.WriteLine("Collision Enter");
        }

        private void OnCollisionStay(BoxCollider other)
        {
            CheckCollision(other);
            //Console.WriteLine("Collision Stay!");
        }

        private void OnUpdate(GameTime gameTime)
        {
            mouse = Mouse.GetState();
            transform.Position = new Vector2(mouse.X, mouse.Y);
            UpdateLoop(gameTime);
        }

        private void UpdateLoop(GameTime gameTime)
        {
            if (Pin.isBought == true && Pin.mySelection == true)
            {
                ForPin(gameTime);
            }
            else if (Pan.isBought == true && Pan.mySelection == true)
            {
                ForPan(gameTime);
            }
            else
            {
                collider.SetSize(1, 1);
                ForArrow(gameTime);
            }
        }

        private void ForPin(GameTime gameTime)
        {
            if (isRendered == false)
            {
                collider.bounds.Height = 1;
                collider.bounds.Width = 1;
                Console.WriteLine("Height" + collider.bounds.Height + "Width" + collider.bounds.Width);
                isRendered = true;
            }

                spritePath = "Sprites/PinWeapon";
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
        }

        private void ForPan(GameTime gameTime)
        {
            collider.BoundsToRendereSize();

            if (Pan.mySelection == true)
            {
                spritePath = "Sprites/PanWeapon";
                renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
                isRendered = false;
            }
        }

        private void ForArrow(GameTime gameTime)
        {
            if (isRendered == true)
            {
                collider.SetSize(1, 1);
                isRendered = false;
            }
            spritePath = "Sprites/Arrow";
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
        }

        private void CheckCollision(BoxCollider other)
        {
            mouse = Mouse.GetState();

            //Console.WriteLine(other.GameObject.Tag);

            if (mouse.LeftButton == ButtonState.Released)
            {
                isReleased = true;
            }

            if (mouse.LeftButton == ButtonState.Pressed && isReleased == true && other.GameObject.Tag == "Pin" )
            {
                Pin.mySelection = true;
                Pan.mySelection = false;
                isReleased = false;
            }

            if(mouse.LeftButton == ButtonState.Pressed && isReleased == true && other.GameObject.Tag == "Pan")
            {
                Pan.mySelection = true;
                Pin.mySelection = false;
                isReleased = false;
            }

            if (mouse.LeftButton == ButtonState.Pressed && isReleased == true)
            {
                if (Pin.mySelection == true && Pin.isBought == true)
                {
                    PinWeapon.Attack(other);
                    isReleased = false;
                }
                else if(Pan.mySelection == true && Pan.isBought == true)
                {
                    PanWeapon.Attack(other);
                    isReleased = false;
                }
                else
                {
                    isReleased = false;
                    Health otherHealth = other.GameObject.GetComponent<Health>();
                    otherHealth.RemoveHealth(1);
                }
            }
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;
            base.Destroy();     
        }
    }
}
