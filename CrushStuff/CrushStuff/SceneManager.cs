﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrushStuff
{
    class SceneManager
    {
        public static Scene CurrentScene;

        public static void LoadScene<T>() where T : Scene, new()
        {
            CurrentScene = new T();
            CurrentScene.LoadContent();
        }

        public static void Update(GameTime gameTime)
        {
            if (CurrentScene == null)
                return;

            CurrentScene.Update(gameTime);
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            if (CurrentScene == null)
                return;

            CurrentScene.Draw(spriteBatch);
        }
    }
}
