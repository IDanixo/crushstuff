﻿using System;
using MiniEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrushStuff
{
    class PanWeapon : GameObject
    {
        MouseState mouse;

        Transform transform;
        Renderer renderer;
        //BoxCollider collider;
        Texture2D texture2D;

        private string spritePath;
        public static bool mySelection;
        private bool isBought = false;
        private int cost = 100;

        public PanWeapon()
        {
            Tag = "PanWeapon";

            transform = AddComponent<Transform>();

            renderer = AddComponent<Renderer>();

            EventManager.OnUpdate += OnUpdate;
        }

        public void Start()
        {
            spritePath = "Sprites/PinWeapon";
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
        }

        public void End()
        {
            spritePath = "Sprites/Nichts";
            renderer.SetImage(Managers.Content.Load<Texture2D>(spritePath));
        }

        public static void Attack(BoxCollider other)
        {
            Health health = other.GameObject.GetComponent<Health>();
            health.RemoveHealth(WorldSetting.PanDamage);
        }

        private void OnUpdate(GameTime gameTime)
        {
            mouse = Mouse.GetState();
            transform.Position = new Vector2(mouse.X, mouse.Y);
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;

            base.Destroy();
        }

    }
}
